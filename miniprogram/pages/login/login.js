// pages/login/login.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userinfor:'',
     passinfor:' ',
     tell:''
  },
  login(a){
    console.log(a.detail.value);
    if(a.detail.value.user==''){
      this.setData({
        userinfor:'用户名不能为空'
      })
      return;
    }else if(a.detail.value.pass==''){
      this.setData({
        userinfor:'请输入密码'
      })
      return;
    }else{
      this.setData({
        userinfor:'',
        passinfor:''
      })
    }
    const db = wx.cloud.database()
    db.collection('UserInfo').where({
      user:a.detail.value.user,
      pass:a.detail.value.pass
    })
    .get({
      success: function(res) {
        // res.data 是包含以上定义的两条记录的数组
        console.log(res.data)

        if(res.data.length==1){
          console.log(111)
          wx.navigateTo({
            url: '../../pages/moic/moic',
          })
        }else{
          this.setData({ 
            tell:'账号或者密码错误，请重新输入'
          })
        }

      }
    })   
 },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})