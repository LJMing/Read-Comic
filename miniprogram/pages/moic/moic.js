// pages/moic/moic.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    navbar:['发现','推荐'],
    currentTab:0,
    currenttab:0,
    tap:['投稿推荐','连载更新'],
    listimg:["cloud://qzt-k4icz.717a-qzt-k4icz-1302708372/s1.webp.h","cloud://qzt-k4icz.717a-qzt-k4icz-1302708372/s2.webp.h","cloud://qzt-k4icz.717a-qzt-k4icz-1302708372/s3.webp.h","cloud://qzt-k4icz.717a-qzt-k4icz-1302708372/s4.webp.h","cloud://qzt-k4icz.717a-qzt-k4icz-1302708372/s5.webp.h"]
  },

  // 链接到登陆注册
  sign(){
    wx.navigateTo({
      url: '../sign/sign',
    })
  },
  // 改变导航条的tab
  navbarTap: function(e){
    console.log(e)
    this.setData({
      currentTab:e.currentTarget.dataset.id
    })
  },
  navtap: function(e){
    console.log(e)
    this.setData({
      currenttab:e.currentTarget.dataset.id1
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    const db=wx.cloud.database();
    db.collection('moic').where({})
    .get({
      success:(res)=>{
        console.log(res.data);
        this.setData({
          list:res.data
        
        })
        console.log(1111)
         
      }
    })

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})