// pages/sign/sign.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
     userinfor:'',
     passinfor:' '
  },
  sign(a){
    console.log(a.detail.value);
    if(a.detail.value.user==''){
      this.setData({
        userinfor:'用户名不能为空'
      })
      return;
    }else if(a.detail.value.pass==''){
      this.setData({
        userinfor:'请输入密码'
      })
      return;
    }else{
      this.setData({
        userinfor:'',
        passinfor:''
      })
    }
    const db = wx.cloud.database()
    db.collection('UserInfo').add({
      // data 字段表示需新增的 JSON 数据
      data: {
        // _id: 'todo-identifiant-aleatoire', // 可选自定义 _id，在此处场景下用数据库自动分配的就可以了
        user:a.detail.value.user,
        pass:a.detail.value.pass
        
      },
      success: function(res) {
        // res 是一个对象，其中有 _id 字段标记刚创建的记录的 id
        console.log(res)
      }
    })
    
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})