# Read-Comic

#### 介绍
快看漫画的小程序版本仿写页面，主要有四个页面，主页，发现，推荐，登陆注册，该项目也是基于CSS与HTML布局主要有轮播，tab栏，模板，传参，登陆注册的验证等等功能与技术完成

#### 软件架构
软件架构说明
微信开发者工具开发的小程序


#### 使用说明
1.开始进入第一个界面为注册(若有账号可以点击进入登陆界面)
注册用到了add()函数，登陆用到了find()具体代码在pages/login和page/sign
如果密码或者账号为空，错误等问题都会有提醒，重新输入。
如果注册成功会跳转到登陆界面，登陆成功会进入主页面


![注册页面](https://images.gitee.com/uploads/images/2020/0729/003104_f1649c7b_5405197.png "58aceb4b4c028bd680fe91141a816b0.png")
![登陆页面](https://images.gitee.com/uploads/images/2020/0729/003150_92647da7_5405197.png "08411f615c59280638dbeb8f5850ceb.png")

2.进入主页面，有搜索框，轮播，tab栏，相似结构的数据以及变动性大的数据，将其闯入云数据库里，再引入页面中。
其中用到了较多的模块进行数据和页面的渲染，相似的模块里面还有tab栏，和换一换，都可以用模块，其他的页面跳转后的结构相似

![输入图片说明](https://images.gitee.com/uploads/images/2020/0729/004551_ec08f176_5405197.png "f6b9379823d8a536c3f70a24c94c639.png")<br>
![输入图片说明](https://images.gitee.com/uploads/images/2020/0729/004759_cbd5eede_5405197.png 
"ae3f9502b004a0c47bd965b1c23b131.png")<br>


![输入图片说明](https://images.gitee.com/uploads/images/2020/0729/004902_02997445_5405197.png 
"9e9a93def157961c04940d07be57ab4.png")<br>
![输入图片说明](https://images.gitee.com/uploads/images/2020/0729/005423_efdf19fa_5405197.png "4257ac217d57723d0678f0eea8b5ff7.png")<br>
![输入图片说明](https://images.gitee.com/uploads/images/2020/0729/005241_8758b40f_5405197.png "d16a4c8aedc7535d1935059dbd00c4e.png")<br>









